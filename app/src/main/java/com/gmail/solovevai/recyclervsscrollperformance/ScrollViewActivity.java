package com.gmail.solovevai.recyclervsscrollperformance;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.List;

/**
 * TODO: Add destription
 *
 * @author solovevai on 09.11.16.
 */

public class ScrollViewActivity extends Activity {

    private LinearLayout content;
    private LayoutInflater inflater;

    private boolean firstStart = true;
    private long startTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        startTime = System.currentTimeMillis();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scroll_view);
        setTitle("ScrollView");
        content = (LinearLayout) findViewById(R.id.content);
        inflater = LayoutInflater.from(this);
        List<ModelItem> model = ModelHelper.createModel(100);
        for (ModelItem modelItem : model) {
            bind(modelItem);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (firstStart) {
            firstStart = false;
            long delta = System.currentTimeMillis() - startTime;
            Toast.makeText(this, "ScrollView start time " + delta, Toast.LENGTH_LONG).show();
        }
    }

    public void bind(ModelItem item) {
        if (item instanceof EditTextItem) {
            bindEditText((EditTextItem) item);
        } else if (item instanceof CheckboxItem) {
            bindCheckBox((CheckboxItem) item);
        } else if (item instanceof SpinnerItem) {
            bindSpinner((SpinnerItem) item);
        } else {
            bindComplexItem((ComplexItem) item);
        }
    }

    public void bindEditText(EditTextItem item) {
        View view = inflater.inflate(R.layout.item_edittext, content, false);
        content.addView(view);
        new MyAdapter.EditTextHolder(view).bind(item);
    }

    public void bindCheckBox(CheckboxItem item) {
        View view = inflater.inflate(R.layout.item_checkbox, content, false);
        content.addView(view);
        new MyAdapter.CheckBoxHolder(view).bind(item);
    }

    public void bindSpinner(SpinnerItem item) {
        View view = inflater.inflate(R.layout.item_spinner, content, false);
        content.addView(view);
        new MyAdapter.SpinnerHolder(view).bind(item);
    }

    public void bindComplexItem(ComplexItem item) {
        View view = inflater.inflate(R.layout.item_very_complex, content, false);
        content.addView(view);
        new MyAdapter.ComplexHolder(view).bind(item);
    }
}
