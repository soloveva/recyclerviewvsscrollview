package com.gmail.solovevai.recyclervsscrollperformance;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

/**
 * TODO: Add destription
 *
 * @author solovevai on 16.11.16.
 */

public class ListViewActivity extends Activity {

    private boolean firstStart = true;
    private long startTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        startTime = System.currentTimeMillis();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);
        setTitle("ListView");
        ListView listView = (ListView) findViewById(R.id.listview);
        listView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        ListViewAdapter adapter = new ListViewAdapter();
        adapter.setData(ModelHelper.createModel(100));
        listView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (firstStart) {
            firstStart = false;
            long delta = System.currentTimeMillis() - startTime;
            Toast.makeText(this, "ListView start time " + delta, Toast.LENGTH_LONG).show();
        }
    }



    public static class ListViewAdapter extends BaseAdapter {
        private List<ModelItem> data;
        private LayoutInflater inflater;

        public void setData(List<ModelItem> data) {
            this.data = data;
            notifyDataSetChanged();
        }

        private void setInflater(Context context) {
            if (inflater == null) {
                inflater = LayoutInflater.from(context);
            }
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int i) {
            return data.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public int getViewTypeCount() {
            return 4;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            MyAdapter.ModelViewHolder holder;
            if (view == null) {
                int viewType = getItemViewType(i);
                setInflater(viewGroup.getContext());
                if (viewType == MyAdapter.EDITTEXT_TYPE) {
                    view = inflater.inflate(R.layout.item_edittext, viewGroup, false);
                    holder = new MyAdapter.EditTextHolder(view);
                    view.setTag(holder);
                } else if (viewType == MyAdapter.CHECKBOX_TYPE) {
                    view = inflater.inflate(R.layout.item_checkbox, viewGroup, false);
                    holder = new MyAdapter.CheckBoxHolder(view);
                    view.setTag(holder);
                } else if (viewType == MyAdapter.SPINNER_TYPE) {
                    view = inflater.inflate(R.layout.item_spinner, viewGroup, false);
                    holder = new MyAdapter.SpinnerHolder(view);
                    view.setTag(holder);
                } else {
                    view = inflater.inflate(R.layout.item_very_complex, viewGroup, false);
                    holder = new MyAdapter.ComplexHolder(view);
                    view.setTag(holder);
                }
            } else {
                holder = (MyAdapter.ModelViewHolder) view.getTag();
            }
            holder.bind((ModelItem) getItem(i));
            return view;
        }

        public int getItemViewType(int position) {
            if (getItem(position) instanceof EditTextItem) {
                return MyAdapter.EDITTEXT_TYPE;
            } else if (getItem(position) instanceof CheckboxItem) {
                return MyAdapter.CHECKBOX_TYPE;
            } else if (getItem(position) instanceof SpinnerItem) {
                return MyAdapter.SPINNER_TYPE;
            }
            return MyAdapter.COMPLEX_TYPE;
        }
    }
}
