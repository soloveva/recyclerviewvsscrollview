package com.gmail.solovevai.recyclervsscrollperformance;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.AsyncLayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.List;

/**
 * TODO: Add destription
 *
 * @author solovevai on 17.11.16.
 */

public class ScrollViewAsyncActivity extends Activity {
    private LinearLayout content;
    private AsyncLayoutInflater inflater;

    private boolean firstStart = true;
    private long startTime;

    List<ModelItem> model;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        startTime = System.currentTimeMillis();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scroll_view);
        setTitle("ScrollView");
        content = (LinearLayout) findViewById(R.id.content);
        inflater = new AsyncLayoutInflater(this);
        model = ModelHelper.createModel(100);
        for (int i = 0; i < 20; i++) {
            bind(model.get(i));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (firstStart) {
            firstStart = false;
            long delta = System.currentTimeMillis() - startTime;
            Toast.makeText(this, "ScrollViewAsync start time "  + delta, Toast.LENGTH_LONG).show();
            for (int i = 20; i < 100; i++) {
                bind(model.get(i));
            }
        }
    }

    public void bind(ModelItem item) {
        if (item instanceof EditTextItem) {
            bindEditText((EditTextItem) item);
        } else if (item instanceof CheckboxItem) {
            bindCheckBox((CheckboxItem) item);
        } else if (item instanceof SpinnerItem) {
            bindSpinner((SpinnerItem) item);
        } else {
            bindComplexItem((ComplexItem) item);
        }
    }

    public void bindEditText(final EditTextItem item) {
        inflater.inflate(
            R.layout.item_edittext, content, new AsyncLayoutInflater.OnInflateFinishedListener() {
                @Override
                public void onInflateFinished(View view, int resid, ViewGroup parent) {
                    parent.addView(view);
                    new MyAdapter.EditTextHolder(view).bind(item);
                }
            });
    }

    public void bindCheckBox(final CheckboxItem item) {
        inflater.inflate(
            R.layout.item_checkbox, content, new AsyncLayoutInflater.OnInflateFinishedListener() {
                @Override
                public void onInflateFinished(View view, int resid, ViewGroup parent) {
                    parent.addView(view);
                    new MyAdapter.CheckBoxHolder(view).bind(item);
                }
            });
    }

    public void bindSpinner(final SpinnerItem item) {
        inflater.inflate(
            R.layout.item_spinner, content, new AsyncLayoutInflater.OnInflateFinishedListener() {
                @Override
                public void onInflateFinished(View view, int resid, ViewGroup parent) {
                    parent.addView(view);
                    new MyAdapter.SpinnerHolder(view).bind(item);
                }
            });
    }

    public void bindComplexItem(final ComplexItem item) {
        inflater.inflate(
            R.layout.item_very_complex, content, new AsyncLayoutInflater.OnInflateFinishedListener() {
                @Override
                public void onInflateFinished(View view, int resid, ViewGroup parent) {
                    parent.addView(view);
                    new MyAdapter.ComplexHolder(view).bind(item);
                }
            });
    }
}
