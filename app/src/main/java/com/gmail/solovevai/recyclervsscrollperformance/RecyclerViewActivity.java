package com.gmail.solovevai.recyclervsscrollperformance;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

/**
 * TODO: Add destription
 *
 * @author solovevai on 15.11.16.
 */

public class RecyclerViewActivity extends Activity {

    private boolean firstStart = true;
    private long startTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        startTime = System.currentTimeMillis();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);
        setTitle("RecyclerView");
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        MyAdapter adapter = new MyAdapter();
        adapter.setData(ModelHelper.createModel(100));
        recyclerView.setAdapter(adapter);
       // recyclerView.setItemViewCacheSize(100);
       // recyclerView.setFocusableInTouchMode(true);
/*        recyclerView.getRecycledViewPool().setMaxRecycledViews(MyAdapter.EDITTEXT_TYPE,20);
        recyclerView.getRecycledViewPool().setMaxRecycledViews(MyAdapter.CHECKBOX_TYPE,0);
        recyclerView.getRecycledViewPool().setMaxRecycledViews(MyAdapter.COMPLEX_TYPE,0);
        recyclerView.getRecycledViewPool().setMaxRecycledViews(MyAdapter.SPINNER_TYPE,0);*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (firstStart) {
            firstStart = false;
            long delta = System.currentTimeMillis() - startTime;
            Toast.makeText(this, "RecyclerView start time " + delta, Toast.LENGTH_LONG).show();
        }
    }
}
