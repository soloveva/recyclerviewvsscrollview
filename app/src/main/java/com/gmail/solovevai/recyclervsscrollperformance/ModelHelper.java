package com.gmail.solovevai.recyclervsscrollperformance;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: Add destription
 *
 * @author solovevai on 09.11.16.
 */

public class ModelHelper {

    public static List<ModelItem> createModel(int size) {
        List<ModelItem> model = new ArrayList<>(size);
        ArrayList<String> list = new ArrayList<>();
        list.add("A");
        list.add("B");
        list.add("C");
        for (int i = 0; i < size; i++) {
            if (i%2 == 0) {
                model.add(new EditTextItem("edittext " + i));
            } else if (i%5==0) {
                model.add(new ComplexItem("text1 " + i, "text2 " + i, "text3 " + i));
            } else if (i%3 == 0) {
                model.add(new SpinnerItem(list));
            } else {
                model.add(new CheckboxItem("checkbox " + i));
            }
        }
        return model;
    }
}
