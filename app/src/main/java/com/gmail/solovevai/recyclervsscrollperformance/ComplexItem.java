package com.gmail.solovevai.recyclervsscrollperformance;

/**
 * TODO: Add destription
 *
 * @author solovevai on 15.11.16.
 */

public class ComplexItem extends ModelItem{
    public final String text1;
    public final String text2;
    public final String text3;

    public ComplexItem(String text1, String text2, String text3) {
        this.text1 = text1;
        this.text2 = text2;
        this.text3 = text3;
    }
}
