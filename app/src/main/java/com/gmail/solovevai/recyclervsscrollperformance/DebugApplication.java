package com.gmail.solovevai.recyclervsscrollperformance;

import android.app.Application;
import com.codemonkeylabs.fpslibrary.TinyDancer;

/**
 * TODO: Add destription
 *
 * @author solovevai on 17.11.16.
 */

public class DebugApplication extends Application {

    @Override public void onCreate() {
        super.onCreate();
        TinyDancer.create().show(this);
    }
}
