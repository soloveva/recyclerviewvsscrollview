package com.gmail.solovevai.recyclervsscrollperformance;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.List;

/**
 * TODO: Add destription
 *
 * @author solovevai on 09.11.16.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ModelViewHolder> {
    public static final int EDITTEXT_TYPE = 0;
    public static final int CHECKBOX_TYPE = 1;
    public static final int SPINNER_TYPE = 2;
    public static final int COMPLEX_TYPE = 3;
    private List<ModelItem> data;
    private LayoutInflater inflater;

    public void setData(List<ModelItem> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public ModelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        setInflater(parent.getContext());
        if (viewType == EDITTEXT_TYPE) {
            EditTextHolder editTextHolder = new EditTextHolder(inflater.inflate(R.layout.item_edittext, parent, false));
            //editTextHolder.setIsRecyclable(false);
            return editTextHolder;
        } else if (viewType == CHECKBOX_TYPE) {
            return new CheckBoxHolder(inflater.inflate(R.layout.item_checkbox, parent, false));
        } else if (viewType == SPINNER_TYPE) {
            return new SpinnerHolder(inflater.inflate(R.layout.item_spinner, parent, false));
        } else {
            return new ComplexHolder(inflater.inflate(R.layout.item_very_complex, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(ModelViewHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        if (data.get(position) instanceof EditTextItem) {
            return EDITTEXT_TYPE;
        } else if (data.get(position) instanceof CheckboxItem) {
            return CHECKBOX_TYPE;
        } else if (data.get(position) instanceof SpinnerItem) {
            return SPINNER_TYPE;
        }
        return COMPLEX_TYPE;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private void setInflater(Context context) {
        if (inflater == null) {
            inflater = LayoutInflater.from(context);
        }
    }

    public static abstract class ModelViewHolder<T extends ModelItem> extends RecyclerView.ViewHolder {

        public ModelViewHolder(View itemView) {
            super(itemView);
        }

        public abstract void bind(T item);
    }

    public static class EditTextHolder extends ModelViewHolder<EditTextItem> {
        private TextView nameView;
        private EditText value;

        public EditTextHolder(View itemView) {
            super(itemView);
            nameView = (TextView) itemView.findViewById(R.id.edittext_name);
            value = (EditText) itemView.findViewById(R.id.edittext_value);
        }

        public void bind(EditTextItem item) {
            nameView.setText(item.name);
            value.setText(item.value);
        }
    }

    public static class CheckBoxHolder extends ModelViewHolder<CheckboxItem> {

        private CheckBox checkBox;

        public CheckBoxHolder(View itemView) {
            super(itemView);
            this.checkBox = (CheckBox) itemView.findViewById(R.id.checkbox_item);
        }

        public void bind(CheckboxItem item) {
            checkBox.setText(item.name);
            checkBox.setChecked(item.value);
        }
    }

    public static class ComplexHolder extends ModelViewHolder<ComplexItem> {

        private TextView text1;
        private TextView text2;
        private TextView text3;

        public ComplexHolder(View itemView) {
            super(itemView);
            this.text1 = (TextView) itemView.findViewById(R.id.text1);
            this.text2 = (TextView) itemView.findViewById(R.id.text2);
            this.text3 = (TextView) itemView.findViewById(R.id.text3);
        }

        @Override
        public void bind(ComplexItem item) {
            text1.setText(item.text1);
            text2.setText(item.text2);
            text3.setText(item.text3);
        }
    }

    public static class SpinnerHolder extends ModelViewHolder<SpinnerItem> {

        private Spinner spinner;

        public SpinnerHolder(View itemView) {
            super(itemView);
            spinner = (Spinner) itemView.findViewById(R.id.spinner);
        }

        @Override
        public void bind(SpinnerItem item) {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(
                spinner.getContext(), android.R.layout.simple_spinner_item, item.values);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
        }
    }
}
